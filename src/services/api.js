import apisauce from 'apisauce'


const create = (baseURL) => {
  const api = apisauce.create({ baseURL })

  const search = (keyword) => {
    return api.get('/albums')
  }

  const getAlbums = () => api.get('/albums')
  const getPosts = () => api.get('/posts')


  return {
    search,
    getAlbums,
    getPosts,
  }

}

export default {
  create
}