import { createReducer, createActions } from 'reduxsauce'
import Immutable from 'seamless-immutable'

/* ------------- Types and Action Creators ------------- */

const { Types, Creators } = createActions({
  searchOnSearch: ['keyword'],
  searchSetKeyword: ['keyword'],
  searchGetAlbums: null,
  searchGetPosts: null,
  searchSetAlbums: ['albums'],
})

export const SearchTypes = Types
export default Creators

/* ------------- Initial State ------------- */

export const INITIAL_STATE = Immutable({
  keyword: '',
  albums: [],
})

/* ------------- Reducers ------------- */

export const onSearch = (state, { keyword }) => 
  state.merge({ })

export const setKeyword = (state, { keyword }) =>
  state.merge({ keyword })

export const setAlbums = (state, { albums }) =>
  state.merge({ albums })

/* ------------- Hookup Reducers To Types ------------- */

export const reducer= createReducer(INITIAL_STATE, {
  [Types.SEARCH_ON_SEARCH]: onSearch,
  [Types.SEARCH_SET_KEYWORD]: setKeyword,
  [Types.SEARCH_SET_ALBUMS]: setAlbums,
})