import { createStore, applyMiddleware, compose } from 'redux'
import { composeWithDevTools } from 'redux-devtools-extension'
import createSagaMiddleware from 'redux-saga'

export default (rootReducer, rootSaga) => {
  
  const middleware = []
  const enhancers = []
  const sagaMiddleware = createSagaMiddleware()
  middleware.push(sagaMiddleware)

  /* ------------- Assemble Middleware ------------- */
  
  enhancers.push(applyMiddleware(...middleware))

  const composeEnhancers = composeWithDevTools({ realtime: true })
  const store = createStore(rootReducer, composeEnhancers(...enhancers))

  let sagasManager = sagaMiddleware.run(rootSaga)

  return {
    store,
    sagasManager,
    sagaMiddleware
  }

}




