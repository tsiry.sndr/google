import React, { Component } from 'react'
import { connect } from 'react-redux'

class AlbumList extends Component {
  
  render() {
    console.log(this.props.albums)
    return (
      <div>
        {
          this.props.albums.map(item => <div>{item.title}</div>)
        }
      </div>
    )
  }

}

const mapStateToProps = (state) => {
  return {
    albums: state.search.albums
  }
}

const mapDispatchToProps = (dispatch) => {
  return {
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(AlbumList)