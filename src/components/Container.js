import React, { Component } from 'react'
import Logo from './Logo'
import SearchButton from './SearchButton'
import SearchInput from './SearchInput'
import AlbumsList from './AlbumsList'

export default class Container extends Component {
  
  render() {
    return (
      <div>
        <Logo />
        <SearchInput />
        <SearchButton />
        <AlbumsList />
      </div>
    )
  }

}