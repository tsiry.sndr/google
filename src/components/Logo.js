import React, { Component } from 'react'
import { StyledLogo } from './styles/LogoStyle'

export default class Logo extends Component {
  
  render() {
    return (
      <StyledLogo>
        <img src='https://www.google.mg/images/branding/googlelogo/1x/googlelogo_color_272x92dp.png' />
      </StyledLogo>
    )
  }

}