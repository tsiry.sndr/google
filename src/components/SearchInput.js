import React, { Component } from 'react'
import { connect } from 'react-redux'
import SearchActions from '../redux/SearchRedux'

class SearchInput extends Component {
  
  render() {
    return (
      <div style={{ textAlign: 'center' }}>
        <input 
          style={{ width: 500, height: 35, paddingLeft: 15, paddingRight: 15, fontSize: 18 }} 
          value={this.props.keyword}
          onChange={(ev) => this.props.setKeyword(ev.target.value)}
        />
      </div>
    )
  }

}

const mapStateToProps = (state) => {
  return {
    keyword: state.search.keyword
  }
}

const mapDispatchToProps = (dispatch) => {
  return {
    setKeyword: (keyword) => dispatch(SearchActions.searchSetKeyword(keyword))
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(SearchInput)