import styled from 'styled-components'

export const Button = styled.button`
  width: 200px;
  height: 35px;
  margin-top: 20px;
  font-size: 18px;
`