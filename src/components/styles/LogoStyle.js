import styled from 'styled-components'

export const StyledLogo = styled.div`
  display: flex;
  align-items: flex-end;
  justify-content: center;
  height: 40vh;
  margin-bottom: 50px;
`