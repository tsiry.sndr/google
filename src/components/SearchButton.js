import React, { Component } from 'react'
import { Button } from './styles/SearchButtonStyle'
import { connect } from 'react-redux' 
import SearchActions from '../redux/SearchRedux'

class SearchButton extends Component {
  
  render() {
    return (
      <div style={{ textAlign: 'center' }}>
        <Button onClick={() => this.props.getAlbums()}>
          Search
        </Button>
      </div>
    )
  }

}

const mapStateToProps = (state) => {
  return {
    keyword: state.search.keyword
  }
}

const mapDispatchToProps = (dispatch) => {
  return {
    onSearch: (keyword) => dispatch(SearchActions.searchOnSearch(keyword)),
    getAlbums: () => dispatch(SearchActions.searchGetAlbums()),
    getPosts: () => dispatch(SearchActions.searchGetPosts()),
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(SearchButton)