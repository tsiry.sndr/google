import { call, put, select } from 'redux-saga/effects'
import SearchActions from '../redux/SearchRedux'

export function onSearch(api, action) {
  const { keyword } = action
  console.log(keyword)
}

export function * getAlbums(api, action) {
  const response = yield call(api.getAlbums)
  console.log(response)
  const albums = response.data
  yield put(SearchActions.searchSetAlbums(albums))
}

export function * getPosts(api, action) {
  // const keyword = yield select(state => state.search.keyword)
  const resposne = yield call(api.getPosts)
}
