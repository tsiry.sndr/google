import { takeLatest, all } from 'redux-saga/effects'
import API from '../services/api'
import { SearchTypes } from '../redux/SearchRedux'

import { 
  onSearch,
  getAlbums,
  getPosts,
} from './SearchSaga'

const api = API.create('https://jsonplaceholder.typicode.com/')

export default function * root () {
  yield all([

    // some sagas receive extra parameters in addition to an action
    // takeLatest(GithubTypes.USER_REQUEST, getUserAvatar, api)
    takeLatest(SearchTypes.SEARCH_ON_SEARCH, onSearch, api),
    takeLatest(SearchTypes.SEARCH_GET_ALBUMS, getAlbums, api),
    takeLatest(SearchTypes.SEARCH_GET_POSTS, getPosts, api),
  ])
}

